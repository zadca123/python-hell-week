from typing import List
from random import randint


def f(val: int, digits: List[int]) -> bool:
    return val in digits


n = 1_000_000
digits_example = [randint(-1000, 1000) for _ in range(n)]

print(f(1, digits_example))
