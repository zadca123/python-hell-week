def str_to_int(value: str) -> int:
    try:
        value: int = int(value)
    except ValueError:
        raise Exception("invalid input")
    return value


def check_if_in_range(number: int, bot: int = 101, top: int = 100_000) -> bool:
    return number in [num for num in range(bot, top)]


def reverse_number() -> int:
    user_input: str = input("Type number: ")
    if check_if_in_range(str_to_int(user_input)):
        number_reversed = "".join(reversed(user_input))
        return str_to_int(number_reversed)
    else:
        raise Exception("Number not in range")


print(reverse_number())
