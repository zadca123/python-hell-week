from pprint import pprint
import math
from collections import defaultdict


def list_of_figures_from_file(filepath: str) -> list[dict]:
    figures: list[dict] = []
    with open(filepath, "r") as file:
        for line in file:
            name, *values = line.split(",")
            name: str = name.lower()
            values: list[int] = str_to_int(values)
            figures.append({"name": name, "values": values})

    return figures


def str_to_int(values: list[str]) -> list[int]:
    int_list: list[int] = []
    for value in values:
        try:
            int_list.append(int(value))
        except ValueError:
            int_list.append(0)
    return int_list


def calculate_areas(figures: list[dict]) -> dict[list]:
    figure_stats: dict[list] = defaultdict(list)

    for figure in figures:
        name: str = figure["name"]
        values: list[int] = figure["values"]
        area: float = get_appropriate_area(name, values)
        info: dict = {"values": values, "area": area}
        figure_stats[name].append(info)

    print_minmax_area(figure_stats)
    return figure_stats


def print_minmax_area(figures: dict[list]) -> None:
    for name, infos in figures.items():
        min_area, max_area = None, None
        for info in infos:
            area: float = info["area"]
            if min_area is None or min_area > area:
                min_area = area
            if max_area is None or max_area < area:
                max_area = area
        print(f"Max area of a {name}: {max_area}")
        print(f"Min area of a {name}: {min_area}")


def get_appropriate_area(figure_name: str, values: list) -> float:
    if figure_name in ["kolo", "circle"]:
        return area_circle(*values)
    elif figure_name in ["kwadrat", "prostokat", "square", "rectangle"]:
        return area_rectangle(*values)
    elif figure_name in ["trojkat", "triangle"]:
        return area_triangle(*values)
    elif figure_name in ["trapez", "trapezoid"]:
        return area_trapezoid(*values)
    else:
        raise Exception("No such figure, take your medication right now!")


def area_circle(r: int, *args, **kwargs) -> float:
    return math.pi * r**2


def area_rectangle(a: float, b: float = None, *args, **kwargs) -> float:
    if b is None:
        b = a
    return a * b


def area_trapezoid(a: float, b: float, h: float, *args, **kwargs) -> float:
    return (1 / 2) * h * (a + b)


def area_triangle(a: float, h: float, *args, **kwargs) -> float:
    return a * h / 2


figures: list[dict] = list_of_figures_from_file("./figures.txt")
pprint(calculate_areas(figures))
