import csv
from pprint import pprint


def setup_dictionaries(names: list) -> list[dict]:
    return [{"name": name, "points": 0, "scores": []} for name in names]


def str_to_int(value: str) -> int:
    try:
        value: int = int(value)
    except ValueError:
        value: int = 0
    return value


def convert_list(values: list[str]) -> list[int]:
    return [str_to_int(value) for value in values]


def max_value_index(values: list) -> int:
    max_value: int = max(values)
    index: int = values.index(max_value)
    return index


def min_value_index(values: list) -> int:
    min_value: int = min(values)
    index: int = values.index(min_value)
    return index


def load_csv(filepath) -> None:
    with open(filepath, "r") as csvfile:
        csvreader = csv.reader(csvfile)

        names = next(csvreader)
        result = setup_dictionaries(names)

        for num, row in enumerate(csvreader):
            values: list[int] = convert_list(row)
            max_index: int = max_value_index(values)
            min_index: int = min_value_index(values)

            print(f"\nRound {num+1}")
            for index, player in enumerate(result):
                player["scores"].append(values[index])
                print(f"{player['name']} throws for {player['scores'][-1]}")

                if max_index == min_index:
                    print(f"{player['name']} hands over")
                    continue
                if index == max_index:
                    print(f"{player['name']} gets best score: +3")
                    player["points"] += 3
                if index == min_index:
                    print(f"{player['name']} gets worst score: -1")
                    player["points"] -= 1

        print("\nSummary:")
        pprint(result)


load_csv("./scores.csv")
