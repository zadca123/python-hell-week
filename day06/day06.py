def matrix_from_file(filepath: str) -> list[list[int]]:
    matrix: list[list[int]] = []
    with open(filepath, "r") as file:
        for line in file:
            matrix.append([int(num) for num in line.split(",")])
    return matrix


def check_if_magick_matrix(matrix: list[list[int]]) -> bool:
    diag_sum_1: int = calculate_diagonal_sum(matrix)
    rows: int = calculate_rows_sum(matrix, diag_sum_1)

    matrix_transposed: list[list[int]] = list(zip(*matrix))
    diag_sum_2: int = calculate_diagonal_sum(matrix_transposed)
    columns: int = calculate_rows_sum(matrix_transposed, diag_sum_2)

    return diag_sum_1 == diag_sum_2 == rows == columns


def calculate_rows_sum(matrix: list[list[int]], diag_sum: int) -> int:
    for row in matrix:
        row_sum = sum(row)
        if diag_sum != row_sum:
            return row_sum

    return diag_sum


def calculate_diagonal_sum(matrix: list[list[int]]) -> int:
    cols: int = len(matrix)
    return sum(matrix[rows][cols - rows - 1] for rows in range(cols))


matrix1: list[list[int]] = matrix_from_file(filepath="./magic_square_1.txt")
matrix2: list[list[int]] = matrix_from_file(filepath="./magic_square_2.txt")
# matrix1[0][0] = 1
# matrix2[0][0] = 2
print(check_if_magick_matrix(matrix1))
print(check_if_magick_matrix(matrix2))
