import random


def replay_prompt() -> bool:
    answer: str = input("Do you want to try again? (y/n) :")
    if answer in "yY":
        return True
    return False


def enter_number() -> int:
    number = input("Enter number: ")
    try:
        number = int(number)
    except ValueError:
        raise Exception("You did not typed valid integer!")
    return number


def guess_from_the_range(down_limit, up_limit: int) -> None:
    rand_number: int = random.randint(down_limit, up_limit)
    replay_answer: bool = True
    while replay_answer:
        user_number: int = enter_number()
        if user_number > rand_number:
            print("number too big")
        elif user_number < rand_number:
            print("number too small")
        else:
            print("You guessed correctly!")
            return

        replay_answer: bool = replay_prompt()

    print("end of the game")


guess_from_the_range(0, 256)
