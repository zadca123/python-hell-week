def range_float(stop: float, start: float = 0.0, step: float = 1.0) -> float:
    while start < stop:
        yield start
        start += step


for num in range_float(start=3.5, stop=13.853, step=0.007):
    print(num)
