def user_prompt() -> str:
    letter: str = input("Enter letter or word: ")
    return letter.strip()


def word_match(user_word: str, line: str) -> None:
    if user_word in line.split():
        print(line)


def letter_match(user_letter: str, line: str) -> None:
    for word in line.split():
        letter: str = word[0]
        if user_letter == letter:
            print(word)


def run_action_depending_on_input(user_input: str, line: str) -> None:
    input_len: int = len(user_input)
    if input_len > 1:
        word_match(user_input, line)
    elif input_len == 1:
        letter_match(user_input, line)
    else:
        raise Exception("invalid input")


def read_words_from_file(filepath: str) -> None:
    with open(filepath, "r") as file:
        user_input: str = user_prompt()
        for line in file:
            run_action_depending_on_input(user_input, line)


read_words_from_file(filepath="./content.txt")
